/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteckah.Objetos;

/**
 *
 * @author User
 */
public class Livro {
    private String titulo;
    private String autor;
    private String editora;
    
    public Livro(String t, String a, String e){
        this.autor = a;
        this.editora = e;
        this.titulo = t;
        
    }
    public void setTitulo(String t){
        this.titulo = t;
    }
    public String getTitulo(){
        return titulo;
    }
    public void setAutor(String a){
        this.autor = a;
    }
    public String getAutor(){
        return autor;
    }
    public void setEditora(String e){
        this.editora = e;
    }
    public String getEditora(){
        return editora;
    }

    @Override
    public String toString() {
        return "Livro{" + "Título: " + titulo + ". Autor(a): " + autor + ". Editora: " + editora + '}';
    }
}
