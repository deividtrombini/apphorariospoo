/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteckah.Objetos;

/**
 *
 * @author User
 */
public class Emprestimo{
    private String retirada;
    private String entrega;
    private Aluno aluno;
    private Livro livro;

    public Emprestimo(String retirada, String entrega, Aluno aluno, Livro livro) {
        this.retirada = retirada;
        this.entrega = entrega;
        this.aluno = aluno;
        this.livro = livro;
    }

    public boolean consultarLivro(String t){
        return livro.getTitulo().equals(t);
    }
    public boolean consultarAluno(String n){
        return aluno.getNome().equals(n);
    }
    public String getRetirada() {
        return retirada;
    }

    public void setRetirada(String retirada) {
        this.retirada = retirada;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Livro getLivro() {
        return livro;
    }

    public void setLivro(Livro livro) {
        this.livro = livro;
    }

    public String getEntrega() {
        return entrega;
    }

    public void setEntrega(String entrega) {
        this.entrega = entrega;
    }

    @Override
    public String toString() {
        return "Emprestimo{" + "Retirada: " + retirada + ". Entrega: " + entrega +". "+ aluno.toString() +" "+  livro.toString() + '}';
    }
    
}
