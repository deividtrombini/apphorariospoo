/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteckah.Objetos;

/**
 *
 * @author User
 */
public class Aluno {
    private String nome;
    private String turma;

    public Aluno(String n, String t) {
        this.nome = n;
        this.turma = t;
    }
    
    public void setNome(String n){
        this.nome = n;
    }
    public String getNome(){
        return nome;
    }
    public void setTurma(String t){
        this.turma = t;
    }
    public String getTurma(){
        return turma;
    }

    @Override
    public String toString() {
        return "Aluno{" + "Nome: " + nome + ". Turma: " + turma + '}';
    }
    
}
