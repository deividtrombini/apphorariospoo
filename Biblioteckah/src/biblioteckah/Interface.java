/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteckah;

import biblioteckah.Objetos.Aluno;
import biblioteckah.Objetos.Livro;

/**
 *
 * @author User
 */
public interface Interface {
    public abstract String visualizarTodosAlunos();
    public abstract String visualizarTodosLivros();
    public abstract String visualizarTodosEmprestimos();
    public abstract void addAluno(String nome, String turma);
    public abstract void addLivro(String titulo, String autor, String editora);
    public abstract void addEmprestimo(String retirada, String entrega, Aluno a, Livro l);
    public abstract Aluno buscarAluno(String n);
    public abstract Livro buscarLivro(String t, String e);
    public abstract String buscarEmprestimo(String t);
    public abstract void excluirAluno(String n);
    public abstract void excluirLivro(String t, String a, String e);
    
}
