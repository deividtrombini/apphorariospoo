/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package biblioteckah;

import biblioteckah.Objetos.Aluno;
import biblioteckah.Objetos.Emprestimo;
import biblioteckah.Objetos.Livro;
import java.util.ArrayList;

/**
 *
 * @author User
 */
public class InterfaceComUsuario implements Interface {
    ArrayList<Livro> livros = new ArrayList();
    ArrayList<Aluno> alunos = new ArrayList();
    ArrayList<Emprestimo> emprestimos = new ArrayList();
    Entradas in = new Entradas();
    
    public void menu(){
        Boolean ficar = true;
        do{
        System.out.println("--- POR FAVOR, ESCOLHA UMA OPCAO ---");
        System.out.println(" [1] Adicionar Emprestimo");
        System.out.println(" [2] Visualizar todos Emprestimos");
        System.out.println(" [3] Visualizar todos Alunos");
        System.out.println(" [4] Visualizar todos Livros");
        System.out.println(" [5] Buscar Aluno");
        System.out.println(" [6] Buscar Livro");
        System.out.println(" [7] Buscar Emprestimo");
        System.out.println(" [8] Adicionar Aluno");
        System.out.println(" [9] Adicionar Livro");
        System.out.println(" [0] Sair");
        
        int escolha = in.nextInt(false);
        switch(escolha){
            case 1:
                //ADICIONA UM EMPRESTIMO
                String retirada, entrega, nA, titulo, editora;
                Aluno a;
                Livro l;
                System.out.println("Nome do Aluno:");
                nA = in.nextString();
                a = buscarAluno(nA);
                System.out.println("Título do Livro:");
                titulo = in.nextString();
                System.out.println("Editora:");
                editora = in.nextString();
                l = buscarLivro(titulo, editora);
                System.out.println("Dia da retirada:");
                retirada = in.nextString();
                System.out.println("Dia da entrega:");
                entrega = in.nextString();
                addEmprestimo(retirada, entrega, a, l);
                
                break;
            case 2:
                //VISUALIZAR TODOS EMPRESTIMOS
                System.out.println(visualizarTodosEmprestimos());
                break;
            case 3:
                //VISUALIZAR TODOS ALUNOS
                System.out.println(visualizarTodosAlunos());
                break;
            case 4:
                //VISUALIZAR TODOS LIVROS
                System.out.println(visualizarTodosLivros());
                break;
            case 5:
                //BUSCAR ALUNO
                System.out.println("Nome do Aluno:");
                String nomeA = in.nextString();
                System.out.println(buscarAluno(nomeA));
                break;
            case 6:
                //BUSCAR LIVRO
                System.out.println("Título do Livro:");
                String tituloL = in.nextString();
                System.out.println("Editora do Livro:");
                String editoraL = in.nextString();
                System.out.println(buscarLivro(tituloL, editoraL));
                
                break;
            case 7:
                //BUSCAR EMPRESTIMO
                System.out.println("Título do Livro:");
                String no = in.nextString();
                System.out.println(buscarEmprestimo(no));
                break;
            case 8:
                //ADICIONAR ALUNO
                String nome, turma;
                System.out.println("Nome do Aluno:");
                nome = in.nextString();
                System.out.println("Turma do Aluno:");
                turma = in.nextString();
                addAluno(nome, turma);
                System.out.println("Aluno adicionado com sucesso!");
                break;
            case 9:
                //ADICIONAR LIVRO
                String tit, aut, edit;
                System.out.println("Título do Livro:");
                tit = in.nextString();
                System.out.println("Autor(a) do Livro:");
                aut = in.nextString();
                System.out.println("Editora do Livro:");
                edit = in.nextString();
                addLivro(tit, aut, edit);
                break;
            case 0:
                //SAIR
                ficar = false;
                break;
                
                default:
                    System.out.println("Opção inválida!");
        }
        }while(ficar != false);
    }
    


    @Override
    public void addAluno(String nome, String turma) {
        Aluno novo = new Aluno(nome,turma);
        alunos.add(novo);
    }

    @Override
    public void addLivro(String t, String a, String e) {
        Livro livro = new Livro(t,a,e);
        livros.add(livro);
    }

    @Override
    public void addEmprestimo(String retirada, String entrega, Aluno a, Livro l) {
        
        Emprestimo e = new Emprestimo(retirada, entrega, a , l);
        emprestimos.add(e);
        
    }

    @Override
    public String buscarEmprestimo(String t) {
        for (Emprestimo em : emprestimos) {
                if(em.consultarLivro(t) == true)
                    return em.toString();
            }
        return null;
    }

    @Override
    public void excluirAluno(String n) {
        for (Aluno aluno : alunos) {
            if(aluno.getNome().equals(n))
                alunos.remove(aluno);
        }
    }

    @Override
    public void excluirLivro(String t, String a, String e) {
        for (Livro livro : livros) {
            if(livro.getTitulo().equals(t)){
                if(livro.getAutor().equals(a)){
                    if(livro.getEditora().equals(e)){
                        livros.remove(livro);
                    }
                }
            }
        }
    }

    @Override
    public Aluno buscarAluno(String n) {
        for (Aluno a : alunos) {
            if(a.getNome().equals(n)){
                return a;
            }
        } return null;
    }

    @Override
    public Livro buscarLivro(String t, String e) {
        for (Livro livro : livros) {
            if(livro.getTitulo().equals(t)){
                if(livro.getEditora().equals(e)){
                    return livro;
                }
            }
        }
        return null;
    }

    @Override
    public String visualizarTodosAlunos() {
        return alunos.toString();
    }

    @Override
    public String visualizarTodosLivros() {
        return livros.toString();
    }

    @Override
    public String visualizarTodosEmprestimos() {
        return emprestimos.toString();
    }
    
}
