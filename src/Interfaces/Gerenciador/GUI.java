package Interfaces.Gerenciador;

import Interfaces.IMidiaUsuario;
import Interfaces.Interface;
import Objetos.DiaDaSemana;
import Objetos.Disciplina;
import java.util.ArrayList;

public class GUI implements IMidiaUsuario{
    Interface gerenciador = new Gerenciador();
    EntradasDoUsuarioComValidacao in = new EntradasDoUsuarioComValidacao();

public void menu() {
        boolean ficar = true;
        do {
            System.out.println(gerenciador.visualizar());
            System.out.println("");
            System.out.println("1 - Cadastrar um Horario");
            System.out.println("2 - Excluir um Horario");
            System.out.println("3 - Consultar um dia da semana");
            System.out.println("4 - Editar um dia da semana");
            System.out.println("5 - Sair");
            int escolhaUsuario = in.nextInt(false);
            switch (escolhaUsuario) {
                case 1:
                    cadastro();
                    break;
                case 2:
                    exclusao();
                    break;
                case 3:
                    consulta();
                    break;
                case 4:
                    editar();
                    break;
                case 5:
                    ficar = false;
                    break;
                default:
                    System.out.println("Tente Novamente");
            }
        } while (ficar);
    }
    @Override
    public void cadastro() {
        /*String componenteCurricular, String nomeDoProfessor, String curso, String semestre, String turma,
        boolean notificacar, String dia, String sala, double inicio, double fim*/
        String disciplina, prof,curso, semestre, turma, diaa, sala;
        DiaDaSemana dia = null;
        boolean notificar = true;
        double inicio, fim;
        System.out.println("Disciplina: ");
        disciplina = in.nextString();
        
        System.out.println("Professor(a): ");
        prof = in.nextString();
        System.out.println("Curso: ");
        curso = in.nextString();
        System.out.println("Semestre: ");
        semestre = in.nextString();
        System.out.println("Turma: ");
        turma = in.nextString();
        System.out.println("Notificar: |[1]SIM| |[2]NÃO|");
        int n = in.nextInt(false);
        if(n == 1)
            notificar = true;
        if(n == 2)
            notificar = false;
        dia = this.receberDia();
        System.out.println("Sala: ");
        sala = in.nextString();
        System.out.println("Horário de inicio: ");
        inicio = in.nextDouble(false);
        System.out.println("Horário de término: ");
        fim = in.nextDouble(false);
        Disciplina dis = new Disciplina(disciplina, prof, curso, semestre, turma, notificar, dia, sala, inicio, fim);
        gerenciador.incluir(dis);
    }

    @Override
    public void exclusao() {
        String aula;
        String dia = "";
        System.out.println(gerenciador.visualizar());
        System.out.println("Dia da aula que você deseja excluir: ");
        System.out.println("| [1]Segunda-Feira | [2]Terça-Feira | [3]Quarta-Feira | [4]Quinta-Feira | [5] Sexta-Feira | [6] Sábado |");
            int escdia = in.nextInt(Boolean.FALSE);
            switch(escdia){
                case 1:
                    dia = "Segunda-Feira";
                    break;
                case 2:
                    dia = "Terça-Feira";
                    break;
                case 3:
                    dia = "Quarta-Feira";
                    break;
                case 4:
                    dia = "Quinta-Feira";
                    break;
                case 5:
                    dia = "Sexta-Feira";
                    break;
                case 6:
                    dia = "Sábado";
            }
        
        System.out.println("Digite o nome da Disciplina: ");
        aula = in.nextString();
        
        gerenciador.excluir(dia, aula);

    }

    @Override
    public void consulta() {
        System.out.println("Digite o nome da Disciplina a ser consultado");
        String titulo = in.nextString();
        Disciplina m = gerenciador.consultar(titulo);
        System.out.println(m.toString());
    }

    @Override
    public void editar() {
        /*String componenteCurricular, String nomeDoProfessor, String curso, String semestre, String turma,
        boolean notificacar, String dia, String sala, double inicio, double fim*/
        String disciplina, prof,curso, semestre, turma, sala, diaa;
        DiaDaSemana dia;
        boolean notificar = true;
        double inicio, fim;
        System.out.println("Digite a Disciplina para edição");
        diaa = in.nextString();
        Disciplina velho = (Disciplina)gerenciador.consultar(diaa);
        if(velho != null){
            System.out.println("Disciplina: ");
            disciplina = in.nextString();
            if(disciplina.equals(""))
                disciplina = velho.getNomeDaDisci();
            System.out.println("Professor(a): ");
            prof = in.nextString();
            if(prof.equals(""))
                prof = velho.getNomeProf();
            System.out.println("Curso: ");
            curso = in.nextString();
            if(curso.equals(""))
                curso = velho.getCurso();
            System.out.println("Semestre: ");
            semestre = in.nextString();
            if(semestre.equals(""))
                semestre = velho.getSemestre();
            System.out.println("Turma: ");
            turma = in.nextString();
            if(turma.equals(""))
                turma = velho.getTurma();
            System.out.println("Notificar: [1]SIM | [2]NÃO");
            int n = in.nextInt(false);
            if(n == 1)
                notificar = true;
            if(n == 2)
                notificar = false;
            dia = this.receberDia();
            System.out.println("Sala: ");
            sala = in.nextString();
            if(sala.equals(""))
                sala = velho.getSala();
            System.out.println("Horário de inicio: ");
            inicio = in.nextDouble(false);
            System.out.println("Horário de término: ");
            fim = in.nextDouble(false);
            Disciplina dis = new Disciplina(disciplina, prof, curso, semestre, turma, notificar, dia, sala, inicio, fim);
            gerenciador.editar(velho, dis);
        }
        
    }
    
    public DiaDaSemana receberDia(){
        DiaDaSemana dia = new DiaDaSemana();
        System.out.println("Dia da Semana: (Selecione) ");
            System.out.println("| [1]Segunda-Feira | [2]Terça-Feira | [3]Quarta-Feira | [4]Quinta-Feira | [5] Sexta-Feira | [6] Sábado |");
            int escolha = in.nextInt(Boolean.FALSE);
            switch(escolha){
                case 1:
                    dia.setDia("Segunda-Feira");
                    break;
                case 2:
                    dia.setDia("Terça-Feira");
                    break;
                case 3:
                    dia.setDia("Quarta-Feira");
                    break;
                case 4:
                    dia.setDia("Quinta-Feira");
                    break;
                case 5:
                    dia.setDia("Sexta-Feira");
                    break;
                case 6:
                    dia.setDia("Sábado");
            }
            return dia;
    }

    @Override
    public void carregar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList ordenar(ArrayList a) {
        // falta ainda
        return a;
    }    

}
