package Interfaces.Gerenciador;

import Interfaces.Interface;
import Objetos.Disciplina;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Gerenciador implements Interface {

    ArrayList <Disciplina> aula = new ArrayList();
    EntradasDoUsuarioComValidacao in = new EntradasDoUsuarioComValidacao();

  
    /*public Disciplina encontrarDia(String dia){
        for (Disciplina disciplina : aula) {
            if(disciplina.getDia().equals(dia))
                return disciplina;
        }
        return null;
    }*/
    
    @Override
    public String visualizar() {
        return aula.toString();
        
    }

    @Override
    public void incluir(Disciplina d) {
        aula.add(d);
    }

    @Override
    public Disciplina consultar(String disciplina) {
        for (Disciplina a : aula) {
            if(a.getNomeDaDisci().equals(disciplina)){
                return a;
            }
        }
        return null;
    }

    @Override
    public boolean editar(Disciplina velha, Disciplina nova) {
        for (int i = 0; i < aula.size(); i++) {
            if (velha == aula.get(i)) {
                aula.set(i, nova);             
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean excluir(String dia, String disci ) {
        for (Disciplina d : aula) {
            if(d.getNomeDaDisci().equals(disci)){
                if(d.getDia().equals(dia)){
                    aula.remove(d);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean salvar(String nomeDoArquivo, Gerenciador gerenciador) {
        try {
            FileOutputStream fos = new FileOutputStream(nomeDoArquivo);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(gerenciador);
            fos.close();
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
        
    }

    @Override
    public Gerenciador carregar(String nomeDoArquivo) {
        FileInputStream fis;
        Gerenciador gerenciador = new Gerenciador();
        try {
            fis = new FileInputStream(nomeDoArquivo);
            ObjectInputStream ois = new ObjectInputStream(fis);
            gerenciador = (Gerenciador)ois.readObject();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
     return gerenciador;   
    }
}