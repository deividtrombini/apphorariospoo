package Interfaces;

import Interfaces.Gerenciador.Gerenciador;
import Objetos.Disciplina;

/**
 *
 * @author User
 */
public interface Interface {
    
    public abstract String visualizar();
    public abstract void incluir(Disciplina disciplina);
    public abstract Disciplina consultar(String disciplina);
    public abstract boolean editar(Disciplina velha, Disciplina nova);
    public abstract boolean excluir(String dia, String aula);
    public abstract boolean salvar(String nomeDoArquivo, Gerenciador gerenciador);
    public abstract Gerenciador carregar(String nomeDoArquivo);
}
