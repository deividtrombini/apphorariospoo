package Interfaces;

import java.util.ArrayList;

public interface IMidiaUsuario {

	public abstract void cadastro();

	public abstract void exclusao();

	public abstract void consulta();

	public abstract void editar();
        
	public abstract ArrayList ordenar(ArrayList a);

	public abstract void carregar();

}
