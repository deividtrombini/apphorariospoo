/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

/**
 *
 * @author User
 */
public class DiaDaSemana {
    private String dia;

    public DiaDaSemana() {
        dia = "";
    }

    public String getDAY() {
        return dia.toString();
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    @Override
    public String toString() {
        return dia;
    }
    
}
