package Objetos;

/* Uma aula possui dia da semana, sala, hor�rio de in�cio e hor�rio de fim */
public class Aula {
    private DiaDaSemana dia;
    private String sala;
    private double inicio;
    private double fim;

    public Aula(DiaDaSemana dia, String sala, double inicio, double fim) {
        this.dia = dia;
        this.sala = sala;
        this.inicio = inicio;
        this.fim = fim;
    }

    public String getDia() {
        return dia.getDAY();
    }

    public void setDia(String dia) {
        this.dia.setDia(dia);
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public double getInicio() {
        return inicio;
    }

    public void setInicio(double inicio) {
        this.inicio = inicio;
    }

    public double getFim() {
        return fim;
    }

    public void setFim(double fim) {
        this.fim = fim;
    }

    @Override
    public String toString() {
        return "Aula{" + "dia=" + dia + ", sala=" + sala + ", inicio=" + inicio + ", fim=" + fim + '}';
    }
    
}
