package Objetos;

/* Um componente curricular (disciplina) � composto pelos seguintes campos: nome do
componente curricular, nome do professor, curso, semestre, turma, notifica��o (notificar 15
minutos antes da aula) e uma ou mais aulas associadas. */
public class Disciplina extends Aula{
    private String nomeDaDisci;
    private String nomeProf;
    private String curso;
    private String semestre;
    private String turma;
    private boolean notificacar;

    public Disciplina(String componenteCurricular, String nomeDoProfessor, String curso, String semestre, String turma, boolean notificacar, DiaDaSemana dia, String sala, double inicio, double fim) {
        super(dia, sala, inicio, fim);
        this.nomeDaDisci = componenteCurricular;
        this.nomeProf = nomeDoProfessor;
        this.curso = curso;
        this.semestre = semestre;
        this.turma = turma;
        this.notificacar = notificacar;
    }
    public String getNomeDaDisci() {
        return nomeDaDisci;
    }

    public void setNomeDaDisci(String nomeDaDisci) {
        this.nomeDaDisci = nomeDaDisci;
    }

    public String getNomeProf() {
        return nomeProf;
    }

    public void setNomeProf(String nomeProf) {
        this.nomeProf = nomeProf;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public boolean isNotificacar() {
        return notificacar;
    }

    public void setNotificacar(boolean notificacar) {
        this.notificacar = notificacar;
    }

    @Override
    public String toString() {
        return " \n Dia {" + super.getDia() + ": Disciplina: " + nomeDaDisci + ", Sala: " +super.getSala() + ", Inicio: " + super.getInicio() + ", Final: " + super.getFim() + ", Professor(a): " + nomeProf + ", Curso: " + curso + ", Semestre: " + semestre + ", Turma: " + turma + '}';
    }
}
